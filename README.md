# Ansible Collection - c2platform.atlassian

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-atlassian/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-atlassian/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-atlassian/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-atlassian/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.gis-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/atlassian/)

C2 Platform collection for Atlassian Jira, Confluence and Bitbucket

## Roles

* [jira](./roles/jira)
* [confluence](./roles/confluence)
* [bitbucket](./roles/bitbucket)

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
[Ansible Galaxy](https://galaxy.ansible.com/ui/repo/published/c2platform/atlassian/docs/)
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.atlassian
ansible-doc -t filter --list c2platform.atlassian
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```
