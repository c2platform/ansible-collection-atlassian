# CHANGELOG

## 1.0.1 ( 2024-03-08 )

* `.gitlab-ci.yml` based on `c2platform.core`.
* [confluence](./roles/confluence/) integrated `c2platform.core.cacerts2` role
  with variable `confluence_cacerts2_certificates` and made SSL / TLS optional
  using `confluence_tls_ssl`.

## 1.0.0 ( 2023-06-16 )

* New pipeline script.

## 0.1.5 ( 2022-06-19 )

* Migration to GitLab.com
* GitLab CI/CD